import os
import yt_dlp
import whisper
import firebase_admin
from flask import Flask, request, jsonify, Response
from dotenv import load_dotenv
from llama_index.embeddings.gradient import GradientEmbedding
from llama_index.llms.gradient import GradientBaseModelLLM
from llama_index.core import ServiceContext,set_global_service_context,SimpleDirectoryReader
from llama_index.core import VectorStoreIndex
from llama_index.vector_stores.astra import AstraDBVectorStore
from llama_index.core.schema import TextNode
from astrapy.db import AstraDB, AstraDBCollection

load_dotenv()

astra_db = AstraDB(token="AstraCS:eeYPzuMwEjDMAcWnPLFbjuEQ:5a8db075ff0ba141c2e3e10b36130c13e150dc308f137d928df963d69d2cd280", api_endpoint="https://15904c12-32d7-4b72-8810-77d114c414cf-us-east-1.apps.astra.datastax.com")

collection = AstraDBCollection(
    collection_name="coba2", astra_db=astra_db
)


llm = GradientBaseModelLLM(
    base_model_slug = "llama2-7b-chat",
    max_tokens = 400,
)

embed_model = GradientEmbedding(
    gradient_access_token = os.environ["GRADIENT_ACCESS_TOKEN"],
    gradient_workspace_id = os.environ["GRADIENT_WORKSPACE_ID"],
    gradient_model_slug = "bge-large",
)

service_context = ServiceContext.from_defaults(
    llm = llm,
    embed_model = embed_model,
    chunk_size = 256,
)

set_global_service_context(service_context)

model = whisper.load_model("small")

astra_db_store = AstraDBVectorStore(
    token="AstraCS:eeYPzuMwEjDMAcWnPLFbjuEQ:5a8db075ff0ba141c2e3e10b36130c13e150dc308f137d928df963d69d2cd280", api_endpoint="https://15904c12-32d7-4b72-8810-77d114c414cf-us-east-1.apps.astra.datastax.com",
    collection_name="coba2",
    embedding_dimension=1024
)

index = VectorStoreIndex.from_vector_store(vector_store=astra_db_store)

query_engine = index.as_query_engine()

def download_audio(link):
    try:
        with yt_dlp.YoutubeDL({'extract_audio': True,
                           'format': 'bestaudio',
                           'outtmpl': 'video.mp3'}) as video:
            info_dict = video.extract_info(link, download=True)
            video_title = info_dict["title"]
            if "ext" in info_dict:  # Check for downloaded audio file extension
                return video_title, f"{video_title}.{info_dict['ext']}"
            else:
                raise ValueError("Audio download failed or no extension found")
    except yt_dlp.utils.DownloadError as e:
        raise Exception(f"YouTube download error: {e}") from e

def transcribe(audio_path):
    try:
        result = model.transcribe('video.mp3', fp16=False)
        return result["text"]
    except Exception as e:
        raise Exception(f"Transcription error: {e}") from e

def save_to_astradb(id, video_title,transcript):
    try:
        document_vector = embed_model.get_text_embedding(transcript)
        collection.insert_one(
            {
                "_id": id,
                "$vector": document_vector,
                "text": transcript
            }
        )
    
        return jsonify({"message": "Saved to Astra DB successfully"})
    except Exception as e:
        raise Exception(f"Astra DB error: {e}") from e

app = Flask(__name__)

@app.route("/transcribe", methods=["POST"])
def transcribe_from_youtube():
    youtube_link = request.form.get("youtube_link")
    lesson_id = request.form.get("lesson_id")
    if not youtube_link:
        return jsonify({"error": "Please provide a YouTube link"}), 400

    try:
        video_title, audio_path = download_audio(youtube_link)
        transcript = transcribe(audio_path)
        response = save_to_astradb(lesson_id,video_title,transcript)
        return response, 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500


@app.route("/chat", methods=["POST"])
def chat():
    req_json = request.get_json()
    message = req_json["message"]
    lesson_id = req_json["lesson_id"]

    if not message:
        return jsonify({"error": "Please provide a Message"}), 400

    try:
        
        
        return jsonify({"output": "Hello World"}),200
    except Exception as e:
        return jsonify({"error": str(e)}), 500


@app.route("/")
def hello_world():
    return "Hello, World!"

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)