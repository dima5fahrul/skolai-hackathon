import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

abstract class BaseStateful<T extends StatefulWidget> extends State<T> {
  @protected
  Future<void> showLoading() {
    return EasyLoading.show(
      dismissOnTap: false,
      maskType: EasyLoadingMaskType.black,
    );
  }

  @protected
  Future<void> hideLoading() {
    return EasyLoading.dismiss();
  }

  @protected
  Future<void> showInfoMessage(
    String message, {
    Duration? duration,
    bool? dismissOnTap,
  }) {
    return EasyLoading.showInfo(
      message,
      duration: duration,
      dismissOnTap: dismissOnTap,
    );
  }

  @protected
  Future<void> showSuccessMessage(
    String message, {
    Duration? duration,
    bool? dismissOnTap,
  }) {
    return EasyLoading.showSuccess(
      message,
      duration: duration,
      dismissOnTap: dismissOnTap,
    );
  }

  @protected
  Future<void> showErrorMesssage(
    String message, {
    Duration? duration,
    bool? dismissOnTap,
    bool useMask = false,
  }) {
    return EasyLoading.showError(
      message,
      duration: duration,
      dismissOnTap: dismissOnTap,
      maskType: useMask ? EasyLoadingMaskType.black : null,
    );
  }

  @protected
  Future<void> hideMessage() {
    return EasyLoading.dismiss();
  }

  @protected
  Widget loadingWidget() {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(16),
      child: const CircularProgressIndicator(),
    );
  }

  @protected
  Widget errorWidget(String message) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(16),
      child: Text(
        message,
        textAlign: TextAlign.center,
        style: const TextStyle(fontSize: 14),
      ),
    );
  }

  @protected
  Widget noDataWidget([String? message]) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(16),
      child: Text(
        message ?? "Tidak ada data",
        textAlign: TextAlign.center,
        style: const TextStyle(fontSize: 14),
      ),
    );
  }
}
