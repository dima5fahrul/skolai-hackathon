import 'package:flutter/material.dart';

import '../utils/app_theme.dart';

class CommonButton extends StatelessWidget {
  final bool isPrimary;
  final String title;
  final VoidCallback onClick;
  final Color? textColor;
  final double? height;

  const CommonButton(
      {super.key,
      this.isPrimary = true,
      this.textColor,
      this.height,
      required this.title,
      required this.onClick});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return InkWell(
      onTap: onClick,
      child: Container(
        height: height ?? 48,
        width: double.maxFinite,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(999)),
            color: isPrimary ? AppTheme.primaryColor : Colors.white),
        child: Center(
          child: Text(title,
              style: TextStyle(
                  color: textColor ?? Colors.white,
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)),
        ),
      ),
    );
  }
}
