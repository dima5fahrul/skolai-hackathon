import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/utils/app_theme.dart';

class ClassItem extends StatefulWidget {
  final String name;
  final String code;
  final String? teacher;
  final bool isTeacher;
  final int? totalLesson;
  final int? totalStudent;
  final String? date;
  const ClassItem(
      {super.key,
      this.totalLesson,
      this.totalStudent,
      this.date,
      required this.name,
      required this.code,
      this.teacher,
      required this.isTeacher});

  @override
  State<ClassItem> createState() => _ClassItemState();
}

class _ClassItemState extends BaseStateful<ClassItem> {
  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context).textTheme;

    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          border: Border.all(color: AppTheme.greyColor)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(widget.name,
                    style: textStyle.titleMedium!
                        .copyWith(fontWeight: FontWeight.w600)),
                Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                    decoration: BoxDecoration(
                        color: AppTheme.primaryColor,
                        borderRadius: BorderRadius.circular(100)),
                    child: Text("${widget.totalLesson} Materi" ?? "",
                        style:
                            textStyle.bodySmall!.copyWith(color: Colors.white)))
              ],
            ),
            const Divider(color: AppTheme.greyColor),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Icon(Icons.person, color: AppTheme.greyColor),
                const SizedBox(width: 8),
                Text('${widget.totalStudent} Murid' ?? "",
                    style: textStyle.bodySmall!
                        .copyWith(color: AppTheme.greyColor)),
                const Spacer(),
                const Icon(Icons.access_time, color: AppTheme.greyColor),
                const SizedBox(width: 8),
                Text(widget.date ?? "",
                    style: textStyle.bodyMedium!
                        .copyWith(color: AppTheme.greyColor)),
              ],
            ),
            const SizedBox(height: 8),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(vertical: 12),
              decoration: BoxDecoration(
                  color: AppTheme.primaryColor.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(100)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(widget.code,
                      textAlign: TextAlign.center,
                      style: textStyle.titleMedium!.copyWith(
                          color: AppTheme.primaryColor,
                          fontWeight: FontWeight.w600)),
                  const SizedBox(width: 8),
                  GestureDetector(
                    onTap: () {
                      showLoading();
                      Clipboard.setData(ClipboardData(text: widget.code));
                      hideLoading();
                      showSuccessMessage("Kode kelas berhasil disalin");
                    },
                    child: const Icon(Icons.copy, color: AppTheme.primaryColor),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
