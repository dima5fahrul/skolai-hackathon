import 'package:flutter/material.dart';

import '../utils/app_theme.dart';

class DesktopWrapper extends StatefulWidget {
  final Widget wrapper;

  const DesktopWrapper({super.key, required this.wrapper});

  @override
  State<DesktopWrapper> createState() => _DesktopWrapperState();
}

class _DesktopWrapperState extends State<DesktopWrapper> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.greyColor,
      body: LayoutBuilder(builder: (context, constraints) {
        double maxWidth = 430.0;

        return Center(
          child: SizedBox(
            width: constraints.maxWidth < maxWidth
                ? constraints.maxWidth
                : maxWidth,
            child: widget.wrapper,
          ),
        );
      }),
    );
  }
}
