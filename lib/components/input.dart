import 'package:flutter/material.dart';

class CommonInput extends StatelessWidget {
  final String? placeholder;
  final Widget? suffixIcon;
  final bool obscure;
  final int? maxLines;
  final Function(String) onChanged;
  const CommonInput(
      {super.key,
      this.placeholder,
      this.suffixIcon,
      this.maxLines = 1,
      this.obscure = false,
      required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        border: const OutlineInputBorder(),
        contentPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        hintText: placeholder,
        suffixIcon: suffixIcon,
      ),
      maxLines: maxLines,
      obscureText: obscure,
      onChanged: onChanged,
    );
  }
}
