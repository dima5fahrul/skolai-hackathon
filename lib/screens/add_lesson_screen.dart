import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/components/desktop_wrapper.dart';
import 'package:uuid/uuid.dart';

import '../components/button.dart';
import '../components/input.dart';

class AddLessonScreen extends StatefulWidget {
  final String classId;
  const AddLessonScreen({super.key, required this.classId});

  @override
  State<AddLessonScreen> createState() => _AddLessonScreenState();
}

class _AddLessonScreenState extends BaseStateful<AddLessonScreen> {
  String _title = '';
  String _description = '';
  String _url = '';
  Map<String, String> _body = {};

  late http.Client _client;

  @override
  void initState() {
    super.initState();
    _client = http.Client();
  }

  Future<void> _submit() async {
    try {
      showLoading();
      final id = const Uuid().v1();
      _body = {'id': id, 'url': _url};
      var body = jsonEncode(_body);
      var header = {"Content-Type": "application/json"};

      final url =
          Uri.parse("${dotenv.env['BASE_URL']!}/extract/youtube/transcript");
      var response = await _client.post(url, body: body, headers: header);

      debugPrint(_body.toString());

      switch (response.statusCode) {
        case 200:
          await FirebaseFirestore.instance.collection('lessons').doc(id).set({
            'id': id,
            'classId': widget.classId,
            'title': _title,
            'description': _description,
            'url': _url,
            'date': DateFormat('d MMMM yyyy').format(DateTime.now()),
            'mindmap_id': '96956307-d958-4b05-bc83-90fb0dd46407',
          });
          hideLoading();
          showSuccessMessage('Berhasil menambah pelajaran');
          Navigator.pop(context);
        default:
          throw Exception(response.reasonPhrase);
      }
    } catch (e) {
      debugPrint(e.toString());
      showErrorMesssage(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        automaticallyImplyLeading: false,
        toolbarHeight: 67,
        leadingWidth: 100,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
        title: const Text('Buat Materi',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Stack(children: [
          Column(
            children: [
              const Text(
                  "Buat materi pada kelas kamu agar para siswa dapat mempelajarinya!",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
              const SizedBox(height: 10),
              const Text(
                  "Untuk membuat materi baru, SkolAI membutuhkan URL video dari YouTube. Untuk mendapatkannya kamu dapat share video materi dari YouTube kamu lalu salin disini.",
                  style: TextStyle(fontSize: 10, fontWeight: FontWeight.w400)),
              const SizedBox(height: 16),
              CommonInput(
                  placeholder: 'Judul Materi',
                  onChanged: (value) => _title = value),
              const SizedBox(height: 16),
              CommonInput(
                  placeholder: 'Link youtube, https://youtube.com/watch...',
                  onChanged: (value) {
                    _url = value;
                  }),
              const SizedBox(height: 16),
              CommonInput(
                  placeholder: 'Masukan deskripsi',
                  maxLines: 4,
                  onChanged: (value) {
                    _description = value;
                  }),
            ],
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: CommonButton(title: 'Kirimkan', onClick: () => _submit()),
          )
        ]),
      ),
    ));
  }
}
