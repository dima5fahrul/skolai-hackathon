import 'package:flutter/material.dart';
import 'package:skloai/components/desktop_wrapper.dart';
import 'package:skloai/utils/app_theme.dart';

import '../components/button.dart';
import '../utils/routes.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context).textTheme;
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: AppTheme.primaryColor,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 42, bottom: 64),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(children: [
                    Text('Selamat Datang',
                        style: textStyle.headlineSmall!.copyWith(
                            fontWeight: FontWeight.w600, color: Colors.white)),
                    const SizedBox(height: 16),
                    Text('Belajar Efektif dengan Flipped Classroom',
                        textAlign: TextAlign.center,
                        style: textStyle.headlineSmall!.copyWith(
                            color: Colors.white, fontWeight: FontWeight.w600))
                  ]),
                  Image.asset('assets/images/undraw_reading.png', height: 300),
                  Text('SkolAI',
                      textAlign: TextAlign.center,
                      style: textStyle.headlineMedium!
                          .copyWith(color: Colors.white)),
                  const SizedBox(height: 30),
                ],
              ),
            ),
            Positioned(
              bottom: 32,
              left: 0,
              right: 0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  CommonButton(
                      title: 'Mulai',
                      isPrimary: false,
                      textColor: AppTheme.primaryColor,
                      onClick: () =>
                          Navigator.pushNamed(context, Routes.loginScreen)),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
