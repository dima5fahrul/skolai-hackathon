import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/components/desktop_wrapper.dart';
import 'package:url_launcher/url_launcher.dart';

class ChatMessage {
  String messageContent;
  String messageType;
  ChatMessage({required this.messageContent, required this.messageType});
}

class ChatScreen extends StatefulWidget {
  final String lessonId;
  const ChatScreen({super.key, required this.lessonId});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends BaseStateful<ChatScreen> {
  late http.Client _client;

  final List<ChatMessage> _listMessages = [];

  String _message = '';
  String _messageToWa = '';
  final TextEditingController _messageController = TextEditingController();

  String _classId = '';
  String _ownerClassId = '';
  String _wa = '';

  @override
  void initState() {
    super.initState();
    _client = http.Client();
  }

  Future<void> _submit() async {
    try {
      _listMessages.add(
        ChatMessage(messageContent: _message, messageType: "sender"),
      );
      setState(() {});
      _listMessages.add(
        ChatMessage(messageContent: _message, messageType: "loading"),
      );
      setState(() {
        _messageToWa = _message;
      });
      Map data = {'id': widget.lessonId, 'question': _message};
      var body = jsonEncode(data);
      final url = Uri.parse("${dotenv.env['BASE_URL']!}/question");
      var response = await _client
          .post(url, body: body, headers: {"Content-Type": "application/json"});
      debugPrint(body);
      debugPrint(response.statusCode.toString());
      switch (response.statusCode) {
        case 200:
          var decodedResponse =
              jsonDecode(utf8.decode(response.bodyBytes)) as Map;
          _listMessages.removeLast();
          String message = _conditionalMessage(decodedResponse['output_text']);
          _listMessages.add(
              ChatMessage(messageContent: message, messageType: 'receiver'));
          _messageController.clear();

        default:
          throw Exception(response.reasonPhrase);
      }
      setState(() {});
    } catch (e) {
      showErrorMesssage(e.toString());
      _listMessages.removeLast();
      setState(() {});
    }
  }

  Future<void> _getClassId() async {
    final snapshot = await FirebaseFirestore.instance
        .collection('lessons')
        .where('id', isEqualTo: widget.lessonId)
        .get();

    if (snapshot.docs.isNotEmpty) {
      var document = snapshot.docs.first;
      var classId = document.data()['classId'];
      _classId = classId;
      setState(() {});
      debugPrint('Class ID: $classId');
    } else {
      debugPrint('Document not found');
    }
  }

  Future<void> _getTeacherId() async {
    final snapshot = await FirebaseFirestore.instance
        .collection('class')
        .where('code', isEqualTo: _classId)
        .get();

    if (snapshot.docs.isNotEmpty) {
      var document = snapshot.docs.first;
      var teacherId = document.data()['teacherId'];
      _ownerClassId = teacherId;
      setState(() {});
      debugPrint('OwnerId ID: $_ownerClassId');
    } else {
      debugPrint('Document not found');
    }
  }

  Future<void> _getTeacherWa() async {
    final snapshot = await FirebaseFirestore.instance
        .collection('users')
        .where('id', isEqualTo: _ownerClassId)
        .get();

    if (snapshot.docs.isNotEmpty) {
      var document = snapshot.docs.first;
      var wa = document.data()['whatsapp'];
      _wa = wa;
      setState(() {});
      debugPrint('wa number: $_wa');
    } else {
      debugPrint('Document not found');
    }
  }

  void _launchWhatsApp({required String phone, required String message}) async {
    showLoading();
    _getClassId();
    _getTeacherId();
    _getTeacherWa();

    String url() => "https://wa.me/$phone?text=$message";

    if (await canLaunch(url())) {
      hideLoading();
      await launch(url());
    } else {
      hideLoading();
      showErrorMesssage('Tidak bisa menghubungi $phone');
      throw 'Tidak bisa menghubungi ${url()}';
    }
  }

  String _conditionalMessage(String text) {
    if (text.contains("answer not available in context.") ||
        text.contains("Maaf,") ||
        text.contains("Sorry,")) {
      if (widget.lessonId.contains('-6c2c-11ef-b2b6-b663e93c6f42')) {
        return 'Maaf, bot tidak bisa menjawab pertanyaan tersebut.';
      }
      return "Maaf, bot tidak bisa menjawab pertanyaan tersebut. Anda bisa langsung menanyakan pertanyaan tersebut melalui WhatsApp pemilik kelas.";
    }

    return text;
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: 67,
        title: Row(
          children: [
            GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Container(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                        color: const Color(0xFFEFF2FF),
                        borderRadius: BorderRadius.circular(12)),
                    child: Icon(Icons.arrow_back,
                        color: Theme.of(context).primaryColor))),
            const SizedBox(width: 14),
            Image.asset('assets/images/logo.png', width: 67, height: 25),
            const SizedBox(width: 8),
            const Icon(
              Icons.verified,
              color: Colors.blue,
              size: 18,
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          _buildList(),
          _buildInput(),
        ],
      ),
    ));
  }

  Widget _buildList() {
    return Container(
      margin: const EdgeInsets.only(bottom: 50),
      child: ListView.builder(
        itemCount: _listMessages.length,
        shrinkWrap: true,
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        itemBuilder: (context, index) {
          return _buildItem(_listMessages[index]);
        },
      ),
    );
  }

  Widget _buildItem(ChatMessage chatMessage) {
    return Container(
      padding: const EdgeInsets.only(left: 14, right: 14, top: 10, bottom: 10),
      child: Align(
        alignment: (chatMessage.messageType == "sender"
            ? Alignment.topRight
            : Alignment.topLeft),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: (chatMessage.messageType != "sender"
                ? Colors.grey.shade200
                : Colors.blue[200]),
          ),
          padding: const EdgeInsets.all(16),
          child: chatMessage.messageType == "loading"
              ? LoadingAnimationWidget.waveDots(size: 64, color: Colors.grey)
              : chatMessage.messageContent.contains(
                      'Maaf, bot tidak bisa menjawab pertanyaan tersebut. Anda bisa langsung menanyakan pertanyaan tersebut melalui WhatsApp pemilik kelas.')
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(chatMessage.messageContent,
                            style: const TextStyle(fontSize: 15)),
                        widget.lessonId.contains('-6c2c-11ef-b2b6-b663e93c6f42')
                            ? const SizedBox()
                            : ElevatedButton(
                                onPressed: () => _launchWhatsApp(
                                    phone: _wa, message: _messageToWa),
                                child: const Text('Hubungi')),
                      ],
                    )
                  : Text(chatMessage.messageContent,
                      style: const TextStyle(fontSize: 15)),
        ),
      ),
    );
  }

  Widget _buildInput() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        padding: const EdgeInsets.only(left: 10, bottom: 10, top: 10),
        height: 60,
        width: double.infinity,
        color: Colors.white,
        child: Row(
          children: <Widget>[
            const SizedBox(width: 15),
            Expanded(
              child: TextField(
                controller: _messageController,
                decoration: const InputDecoration(
                    hintText: "Tulis pertanyaan...",
                    hintStyle: TextStyle(color: Colors.black54),
                    border: InputBorder.none),
                onChanged: (value) => _message = value,
              ),
            ),
            const SizedBox(width: 15),
            FloatingActionButton(
              onPressed: _submit,
              backgroundColor: Theme.of(context).primaryColor,
              elevation: 0,
              child: const Icon(Icons.send, color: Colors.white, size: 18),
            ),
          ],
        ),
      ),
    );
  }
}
