import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/components/desktop_wrapper.dart';
import 'package:skloai/components/search_field.dart';

import '../components/class_item.dart';
import '../utils/routes.dart';

class ClassScreen extends StatefulWidget {
  const ClassScreen({super.key});

  @override
  State<ClassScreen> createState() => _ClassScreenState();
}

class _ClassScreenState extends BaseStateful<ClassScreen> {
  final _db = FirebaseFirestore.instance;
  final _uid = FirebaseAuth.instance.currentUser!.uid;

  Future<QuerySnapshot<Object?>>? _classFuture;

  @override
  void initState() {
    super.initState();

    _classFuture = _getClassJoined();
  }

  Future<List<dynamic>> _getClassJoinedCode() async {
    try {
      final snapshot = await _db.collection('users').doc(_uid).get();
      return snapshot.data()!['class'] as List<dynamic>;
    } catch (e) {
      rethrow;
    }
  }

  Future<QuerySnapshot<Object?>> _getClassJoined() async {
    try {
      final currentClass = await _getClassJoinedCode();
      if (currentClass.isEmpty) {
        throw Exception('Anda belum join kelas');
      } else {
        final snapshot = await _db
            .collection('class')
            .where('code', whereIn: currentClass)
            .get();
        return snapshot;
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<bool> _checkStudent(String uid) async {
    DocumentSnapshot snapshot =
        await FirebaseFirestore.instance.collection('users').doc(uid).get();
    final data = snapshot.data() as Map<String, dynamic>;

    return data['role'] == 'student';
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        automaticallyImplyLeading: false,
        toolbarHeight: 40,
        leadingWidth: 100,
        title: const Text('Kelas',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
      ),
      body: Padding(
          padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SearchField(),
              const SizedBox(height: 20),
              const Text('Kelas yang diikuti',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
              const SizedBox(height: 10),
              _buildList(),
            ],
          )),
    ));
  }

  RefreshIndicator _buildList() {
    return RefreshIndicator(
      onRefresh: () async => setState(() {
        _classFuture = _getClassJoined();
      }),
      child: FutureBuilder<QuerySnapshot<Object?>>(
          future: _classFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return loadingWidget();
            } else if (snapshot.hasError) {
              return errorWidget(snapshot.error.toString());
            } else {
              List<Map<String, dynamic>> list = [];

              QuerySnapshot<Object?> querySnapshot = snapshot.data!;
              for (QueryDocumentSnapshot document in querySnapshot.docs) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                list.add(data);
              }

              if (list.isEmpty) {
                return noDataWidget();
              }

              return ListView.separated(
                padding: const EdgeInsets.only(top: 0),
                shrinkWrap: true,
                itemCount: list.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, Routes.detailClassScreen,
                          arguments: ScreenArgument<Map<String, dynamic>>({
                            'classId': list[index]['code'],
                            'className': list[index]['name'],
                            'isTeacher': false
                          }));
                    },
                    child: ClassItem(
                      name: list[index]['name'],
                      code: list[index]['code'],
                      teacher: list[index]['teacherName'],
                      isTeacher: false,
                    ),
                  );
                },
                separatorBuilder: (context, index) =>
                    const SizedBox(height: 10),
              );
            }
          }),
    );
  }
}
