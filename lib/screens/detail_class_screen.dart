import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/components/desktop_wrapper.dart';

import '../utils/app_theme.dart';
import '../utils/routes.dart';

class DetailClassScreen extends StatefulWidget {
  final Map<String, dynamic> data;

  const DetailClassScreen({super.key, required this.data});

  @override
  State<DetailClassScreen> createState() => _DetailClassScreenState();
}

class _DetailClassScreenState extends BaseStateful<DetailClassScreen> {
  Future<QuerySnapshot<Object?>>? _lessonsFuture;

  @override
  void initState() {
    super.initState();
    _lessonsFuture = _getAllLessons();
  }

  Future<QuerySnapshot<Object?>> _getAllLessons() async {
    try {
      final snapshot = await FirebaseFirestore.instance
          .collection('lessons')
          .where('classId', isEqualTo: widget.data['classId'])
          .get();
      return snapshot;
    } catch (e) {
      rethrow;
    }
  }

  void _refreshLessons() => setState(() {
        _lessonsFuture = _getAllLessons();
      });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        automaticallyImplyLeading: false,
        toolbarHeight: 60,
        leadingWidth: 100,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(widget.data['className'].toString(),
            style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
      ),
      floatingActionButton: widget.data['isTeacher']
          ? FloatingActionButton.extended(
              onPressed: () => Navigator.pushNamed(
                      context, Routes.addLessonScreen,
                      arguments: ScreenArgument<String>(widget.data['classId']))
                  .then((value) => _refreshLessons()),
              label: const Text('Materi'),
              icon: const Icon(Icons.add),
            )
          : null,
      body: Padding(
        padding: const EdgeInsets.only(left: 24, right: 24, bottom: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Daftar Materi",
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
            const SizedBox(height: 10),
            TextField(
              decoration: InputDecoration(
                hintText: 'Cari kelas',
                hintStyle: const TextStyle(color: Colors.grey),
                contentPadding: const EdgeInsets.symmetric(vertical: 0),
                prefixIcon: const Icon(Icons.search, color: Colors.black),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(color: AppTheme.greyColor),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(color: AppTheme.greyColor),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(color: AppTheme.greyColor),
                ),
              ),
            ),
            const SizedBox(height: 10),
            _buildList(size),
          ],
        ),
      ),
    ));
  }

  RefreshIndicator _buildList(Size size) {
    return RefreshIndicator(
      onRefresh: () async => _refreshLessons(),
      child: FutureBuilder<QuerySnapshot<Object?>>(
          future: _lessonsFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return loadingWidget();
            } else if (snapshot.hasError) {
              return errorWidget(snapshot.error.toString());
            } else {
              List<Map<String, dynamic>> list = [];

              QuerySnapshot<Object?> querySnapshot = snapshot.data!;
              for (QueryDocumentSnapshot document in querySnapshot.docs) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                list.add(data);
              }

              if (list.isEmpty) {
                return noDataWidget();
              }

              return SizedBox(
                height: size.height - 216,
                child: ListView.separated(
                    shrinkWrap: true,
                    itemCount: list.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 10),
                    itemBuilder: (context, index) => GestureDetector(
                        onTap: () => Navigator.pushNamed(
                            context, Routes.detailLessonScreen,
                            arguments: ScreenArgument<Map<String, dynamic>>(
                                list[index])),
                        child: _buildItem(
                            title: list[index]['title'],
                            date: list[index]['date']))),
              );
            }
          }),
    );
  }

  Widget _buildItem({required String title, required String date}) => SizedBox(
        width: double.maxFinite,
        height: 100,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              border: Border.all(color: AppTheme.greyColor)),
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(title,
                        style: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600)),
                    const SizedBox(height: 6),
                    Text(date,
                        style: const TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400)),
                  ],
                ),
                Image.asset('assets/images/tasklist.png'),
              ],
            ),
          ),
        ),
      );
}
