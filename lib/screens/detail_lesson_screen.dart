import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../components/desktop_wrapper.dart';
import '../utils/routes.dart';

class DetailLessonScreen extends StatefulWidget {
  final Map<String, dynamic> lesson;
  const DetailLessonScreen({super.key, required this.lesson});

  @override
  State<DetailLessonScreen> createState() => _DetailLessonScreenState();
}

class _DetailLessonScreenState extends BaseStateful<DetailLessonScreen> {
  late YoutubePlayerController _controller;
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final _uid = FirebaseAuth.instance.currentUser!.uid;

  late PlayerState _playerState;
  late YoutubeMetaData _videoMetaData;
  bool _isPlayerReady = false;
  bool _isStudent = false;

  String _extractYoutubeId(String url) {
    final match = RegExp(r'v=([^&]+)').firstMatch(url);
    return match?.group(1) ?? '';
  }

  @override
  void initState() {
    super.initState();
    showLoading();
    _controller = YoutubePlayerController(
        initialVideoId: _extractYoutubeId(widget.lesson['url']),
        flags: const YoutubePlayerFlags(mute: false, autoPlay: true));
    _controller.addListener(_listener);
    _videoMetaData = const YoutubeMetaData();
  }

  @override
  void deactivate() {
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
  }

  Future<void> _checkMindmap() async {
    final snapshot = await _db
        .collection('mindmap')
        .where('lesson_id', isEqualTo: widget.lesson['id'])
        .get();

    debugPrint(_uid);
    debugPrint(widget.lesson['id']);

    _isStudent = await _checkStudent(_uid);

    if (_isStudent) {
      if (snapshot.docs.isNotEmpty) {
        debugPrint('step 1');
        if (snapshot.docs.first['user_id'] == _uid &&
            snapshot.docs.first['lesson_id'] == widget.lesson['id']) {
          debugPrint('step 2');
          Navigator.pushNamed(context, Routes.mindMapScreen,
              arguments: ScreenArgument<Map<String, dynamic>>(
                  snapshot.docs.first.data()));
        }
      } else {
        debugPrint('step 4');
        // showErrorMesssage('Mindmap belum dibuat oleh guru');

        Navigator.pushNamed(context, Routes.noteScreen,
            arguments: ScreenArgument<String>(widget.lesson['id']));
      }
    } else {
      debugPrint('step 5');
      Navigator.pushNamed(context, Routes.mindMapListScreen,
          arguments: ScreenArgument<String>(widget.lesson['id']));
    }
  }

  Future<bool> _checkStudent(String uid) async {
    DocumentSnapshot snapshot =
        await FirebaseFirestore.instance.collection('users').doc(uid).get();
    final data = snapshot.data() as Map<String, dynamic>;

    return data['role'] == 'student';
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
              onPressed: () {
                _controller.pause();
                _checkMindmap();
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16)),
              child: const Icon(Icons.account_tree_outlined)),
          const SizedBox(height: 10),
          FloatingActionButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16)),
              onPressed: () {
                _controller.pause();
                Navigator.pushNamed(context, Routes.chatScreen,
                        arguments: ScreenArgument<String>(widget.lesson['id']))
                    .then((value) => _controller.play());
              },
              child: const Icon(Icons.message_outlined))
        ],
      ),
      appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          automaticallyImplyLeading: false,
          toolbarHeight: 67,
          leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context)),
          title: const Text('Materi',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500))),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: YoutubePlayer(
              controller: _controller,
              showVideoProgressIndicator: true,
              progressIndicatorColor: Colors.amber,
              progressColors: const ProgressBarColors(
                  playedColor: Colors.amber, handleColor: Colors.amberAccent),
              onReady: () {
                hideLoading();
                _isPlayerReady = true;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _videoMetaData.title,
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Flexible(
                      flex: 1,
                      child: Row(
                        children: [
                          const Icon(Icons.timer),
                          const SizedBox(width: 10),
                          Text(_printDuration(_videoMetaData.duration)),
                        ],
                      ),
                    ),
                    const SizedBox(width: 20),
                    Flexible(
                      flex: 2,
                      child: Row(
                        children: [
                          const Icon(Icons.person),
                          const SizedBox(width: 10),
                          Expanded(child: Text(_videoMetaData.author))
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 30),
                const Text(
                  'Deskripsi',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                const SizedBox(height: 10),
                Text(widget.lesson['description']),
              ],
            ),
          )
        ],
      ),
    ));
  }

  String _printDuration(Duration duration) {
    String negativeSign = duration.isNegative ? '-' : '';
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60).abs());
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60).abs());
    return "$negativeSign${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }
}
