import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../components/base_stateful.dart';
import '../components/button.dart';
import '../components/class_item.dart';
import '../components/desktop_wrapper.dart';
import '../utils/app_theme.dart';
import '../utils/routes.dart';

class HomeStudentScreen extends StatefulWidget {
  const HomeStudentScreen({super.key});

  @override
  State<HomeStudentScreen> createState() => _HomeStudentScreenState();
}

class _HomeStudentScreenState extends BaseStateful<HomeStudentScreen> {
  String _name = '';
  String _code = '';

  final _db = FirebaseFirestore.instance;
  final _uid = FirebaseAuth.instance.currentUser!.uid;

  @override
  void initState() {
    super.initState();

    _db.collection('users').doc(_uid).get().then((value) {
      final data = value.data() as Map<String, dynamic>;
      setState(() => _name = data['name']);
    });
  }

  Future<QuerySnapshot<Object?>> _getPublicLessons() async {
    try {
      final snapshot = await _db.collection('publicLessons').get();
      return snapshot;
    } catch (e) {
      rethrow;
    }
  }

  Future<void> _joinClass() async {
    try {
      showLoading();
      bool isClassExist = await _checkClass();
      if (!isClassExist) {
        throw 'Tidak ada kelas';
      }

      List<dynamic> currentClass = await _getClassJoinedCode();
      currentClass.add(_code);

      await _db
          .collection('users')
          .doc(_uid)
          .update({'class': FieldValue.arrayUnion(currentClass)});
      hideLoading();
      setState(() {});
    } catch (e) {
      hideLoading();
      showErrorMesssage('Kode tidak ditemukan');
    }
  }

  Future<bool> _checkClass() async {
    try {
      final snapshot = await _db.collection('class').doc(_code).get();
      return snapshot.exists;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<dynamic>> _getClassJoinedCode() async {
    try {
      final snapshot = await _db.collection('users').doc(_uid).get();
      return snapshot.data()!['class'] as List<dynamic>;
    } catch (e) {
      rethrow;
    }
  }

  Future<QuerySnapshot<Object?>> _getClassJoined() async {
    try {
      final currentClass = await _getClassJoinedCode();
      if (currentClass.isEmpty) {
        throw Exception('Anda belum join kelas');
      } else {
        final snapshot = await _db
            .collection('class')
            .where('code', whereIn: currentClass)
            .get();
        return snapshot;
      }
    } catch (e) {
      rethrow;
    }
  }

  _getTotalStudents() async {
    try {
      final snapshot =
          await _db.collection('users').where('class', isEqualTo: _code).get();
      return snapshot.docs.length;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 24, right: 24, left: 24),
            child: ListView(
              children: [
                _buildHeader(),
                const SizedBox(height: 30),
                _buildPublicLessons(),
                const SizedBox(height: 20),
                _buildJoinClass(),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Kelas Terbaru',
                        style: Theme.of(context).textTheme.titleMedium),
                    GestureDetector(
                      onTap: () {},
                      child: Text('Lihat semua',
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(color: AppTheme.primaryColor)),
                    )
                  ],
                ),
                const SizedBox(height: 10),
                _buildList()
              ],
            ),
          ),
        ],
      ),
    ));
  }

  Widget _buildHeader() {
    final date = DateTime.now();
    final textStyle = Theme.of(context).textTheme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Halo, ${_name.split(' ').last}",
                style: textStyle.headlineSmall!
                    .copyWith(fontWeight: FontWeight.w500)),
            Text(
              style: textStyle.bodyMedium!.copyWith(color: Colors.grey),
              DateFormat('EEEE, d MMMM y').format(date),
            ),
          ],
        ),
        GestureDetector(
          onTap: () => FirebaseAuth.instance.signOut().then((_) =>
              Navigator.pushNamedAndRemoveUntil(
                  context, Routes.splashScreen, (route) => false)),
          child: Container(
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: const Icon(
              Icons.logout,
              color: Colors.red,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildPublicLessons() {
    // return FutureBuilder<QuerySnapshot<Object?>>(
    //     future: _getPublicLessons(),
    //     builder: (context, snapshot) {
    //       if (snapshot.connectionState == ConnectionState.waiting) {
    //         return loadingWidget();
    //       } else if (snapshot.hasError) {
    //         return errorWidget(snapshot.error.toString());
    //       } else {
    //         List<Map<String, dynamic>> list = [];
    //
    //         QuerySnapshot<Object?> querySnapshot = snapshot.data!;
    //         for (QueryDocumentSnapshot document in querySnapshot.docs) {
    //           Map<String, dynamic> data =
    //               document.data()! as Map<String, dynamic>;
    //           list.add(data);
    //         }
    //
    //         if (list.isEmpty) {
    //           return noDataWidget();
    //         }
    //
    //         return CarouselSlider.builder(
    //           itemCount: list.length,
    //           options: CarouselOptions(
    //               aspectRatio: 16 / 13,
    //               viewportFraction: 0.8,
    //               initialPage: 0,
    //               enableInfiniteScroll: true,
    //               reverse: false,
    //               autoPlay: true,
    //               autoPlayInterval: const Duration(seconds: 7),
    //               autoPlayAnimationDuration: const Duration(milliseconds: 800),
    //               autoPlayCurve: Curves.fastOutSlowIn,
    //               enlargeCenterPage: true,
    //               enlargeFactor: 0.3,
    //               scrollDirection: Axis.horizontal),
    //           itemBuilder:
    //               (BuildContext context, int itemIndex, int pageViewIndex) =>
    //                   Material(
    //             borderRadius: BorderRadius.circular(12),
    //             color: Colors.transparent,
    //             child: InkWell(
    //               onTap: () => Navigator.pushNamed(
    //                   context, Routes.detailLessonScreen,
    //                   arguments: ScreenArgument<Map<String, dynamic>>(
    //                       list[itemIndex])),
    //               borderRadius: BorderRadius.circular(12),
    //               child: Ink(
    //                 decoration:
    //                     BoxDecoration(borderRadius: BorderRadius.circular(12)),
    //                 child: Container(
    //                   padding: const EdgeInsets.all(8),
    //                   decoration: BoxDecoration(
    //                       color: Colors.transparent,
    //                       border: Border.all(color: const Color(0xFFCAC4D0)),
    //                       borderRadius: BorderRadius.circular(12.0)),
    //                   child: Align(
    //                       alignment: Alignment.bottomCenter,
    //                       child: Column(
    //                         crossAxisAlignment: CrossAxisAlignment.start,
    //                         children: [
    //                           ClipRRect(
    //                               borderRadius: BorderRadius.circular(8.0),
    //                               child: Image.network(
    //                                   fit: BoxFit.cover,
    //                                   list[itemIndex]['thumbnail'],
    //                                   alignment: Alignment.topCenter)),
    //                           const SizedBox(height: 4),
    //                           Text(list[itemIndex]['title'],
    //                               style: const TextStyle(
    //                                   fontSize: 14,
    //                                   fontWeight: FontWeight.w600)),
    //                           const SizedBox(height: 8),
    //                           Row(
    //                             mainAxisAlignment:
    //                                 MainAxisAlignment.spaceBetween,
    //                             crossAxisAlignment: CrossAxisAlignment.start,
    //                             children: [
    //                               Flexible(
    //                                   flex: 2,
    //                                   child: Text(list[itemIndex]['owner'],
    //                                       style: const TextStyle(
    //                                           fontSize: 12,
    //                                           fontWeight: FontWeight.w500))),
    //                               Text(list[itemIndex]['upload'],
    //                                   style: const TextStyle(
    //                                       fontSize: 12,
    //                                       fontWeight: FontWeight.w400))
    //                             ],
    //                           )
    //                         ],
    //                       )),
    //                 ),
    //               ),
    //             ),
    //           ),
    //         );
    //       }
    //     });

    return Container(
      width: double.infinity,
      height: 180,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: AppTheme.primaryColor),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomRight,
            child: Image.asset(
              'assets/images/undraw_water_grass.png',
              height: 120,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Ingin Melihat materi lain?',
                          style: Theme.of(context)
                              .textTheme
                              .titleLarge!
                              .copyWith(color: Colors.white)),
                      CommonButton(
                          title: 'Pergi',
                          onClick: () {},
                          height: 32,
                          textColor: AppTheme.primaryColor,
                          isPrimary: false)
                    ],
                  ),
                ),
                const SizedBox(width: 16),
                Flexible(
                    flex: 1,
                    child: Image.asset('assets/images/undraw_window.png'))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildJoinClass() {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(12)),
      child: Row(
        children: [
          Expanded(
            flex: 8,
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Masukkan Kode Kelas',
                hintStyle: const TextStyle(color: Colors.grey),
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(color: AppTheme.greyColor)),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(color: AppTheme.greyColor)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(color: AppTheme.greyColor)),
              ),
              onChanged: (value) => _code = value,
            ),
          ),
          const SizedBox(width: 8),
          Expanded(
            flex: 2,
            child: CommonButton(title: 'Join', onClick: () => _joinClass()),
          )
        ],
      ),
    );
  }

  Widget _buildList() {
    return FutureBuilder<QuerySnapshot<Object?>>(
        future: _getClassJoined(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return loadingWidget();
          } else if (snapshot.hasError) {
            return errorWidget(snapshot.error.toString());
          } else {
            List<Map<String, dynamic>> list = [];

            QuerySnapshot<Object?> querySnapshot = snapshot.data!;
            for (QueryDocumentSnapshot document in querySnapshot.docs) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              list.add(data);
            }

            if (list.isEmpty) {
              return noDataWidget();
            }

            return ListView.separated(
              padding: const EdgeInsets.only(top: 0),
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: list.length > 2 ? 2 : list.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, Routes.detailClassScreen,
                        arguments: ScreenArgument<Map<String, dynamic>>({
                          'classId': list[index]['code'],
                          'className': list[index]['name'],
                          'isTeacher': false
                        }));
                  },
                  child: ClassItem(
                    name: list[index]['name'],
                    code: list[index]['code'],
                    teacher: list[index]['teacherName'],
                    isTeacher: false,
                  ),
                );
              },
              separatorBuilder: (context, index) => const SizedBox(height: 10),
            );
          }
        });
  }
}

class _GradientBackgroundView extends StatelessWidget {
  const _GradientBackgroundView();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            AppTheme.primaryColor,
            Colors.white,
          ],
          stops: [0.064, 0.20],
        ),
      ),
    );
  }
}
