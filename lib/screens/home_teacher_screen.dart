import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/utils/app_theme.dart';

import '../components/class_item.dart';
import '../components/desktop_wrapper.dart';
import '../components/input.dart';
import '../utils/routes.dart';

class HomeTeacherScreen extends StatefulWidget {
  const HomeTeacherScreen({super.key});

  @override
  State<HomeTeacherScreen> createState() => _HomeTeacherScreenState();
}

class _HomeTeacherScreenState extends BaseStateful<HomeTeacherScreen> {
  String _name = '';
  List<Map<String, dynamic>> _classList = [];

  final _uid = FirebaseAuth.instance.currentUser!.uid;
  final _db = FirebaseFirestore.instance;

  @override
  void initState() {
    super.initState();
    _db.collection('users').doc(_uid).get().then((value) {
      final data = value.data() as Map<String, dynamic>;
      setState(() => _name = data['name']);
    });

    _loadClasses();
  }

  Future<void> _loadClasses() async {
    try {
      final snapshot = await _db
          .collection('class')
          .where('teacherId', isEqualTo: _uid)
          .get();

      List<Map<String, dynamic>> list = [];
      for (QueryDocumentSnapshot document in snapshot.docs) {
        Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
        list.add(data);
      }

      setState(() {
        _classList = list;
      });
    } catch (e) {
      debugPrint(e.toString());
      showErrorMesssage('Gagal mengambil data');
    }
  }

  Future<QuerySnapshot<Object?>> _getAllClass() async {
    try {
      final snapshot = await _db
          .collection('class')
          .where('teacherId', isEqualTo: _uid)
          .get();
      return snapshot;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton.extended(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          icon: const Icon(Icons.add),
          label: const Text('Kelas'),
          onPressed: () => showDialog(
                  context: context, builder: (_) => const _AddClassDialog())
              .then((value) => setState)),
      body: Stack(
        children: [
          const _GradientBackgroundView(),
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: ListView(
              children: [
                _buildHeader(),
                const SizedBox(height: 20),
                _buildList()
              ],
            ),
          ),
        ],
      ),
    ));
  }

  Widget _buildHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Halo, ${_name.split(' ')[0]}",
                style: const TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                )),
            const Text('Ingin perbarui materi apa hari ini?',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                )),
          ],
        ),
        GestureDetector(
          onTap: () => FirebaseAuth.instance.signOut().then((_) =>
              Navigator.pushNamedAndRemoveUntil(
                  context, Routes.splashScreen, (route) => false)),
          child: Container(
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: const Icon(
              Icons.logout,
              color: Colors.red,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text("Daftar Kelas",
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500)),
            InkWell(
              child: const Icon(Icons.refresh),
              onTap: () => _loadClasses(),
            )
          ],
        ),
        const SizedBox(height: 10),
        TextField(
          decoration: InputDecoration(
            hintText: 'Cari kelas',
            hintStyle: const TextStyle(color: Colors.grey),
            contentPadding: const EdgeInsets.symmetric(vertical: 0),
            prefixIcon: const Icon(Icons.search, color: Colors.black),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(color: AppTheme.greyColor),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(color: AppTheme.greyColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(color: AppTheme.greyColor),
            ),
          ),
        ),
        const SizedBox(height: 16),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.6,
          child: _classList.isEmpty
              ? noDataWidget() // Widget saat data kosong
              : ListView.separated(
                  padding: const EdgeInsets.all(0),
                  shrinkWrap: true,
                  itemCount: _classList.length,
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 16),
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () =>
                          Navigator.pushNamed(context, Routes.detailClassScreen,
                              arguments: ScreenArgument<Map<String, dynamic>>({
                                'classId': _classList[index]['code'],
                                'className': _classList[index]['name'],
                                'isTeacher': true
                              })),
                      child: ClassItem(
                        name: _classList[index]['name'],
                        code: _classList[index]['code'],
                        teacher: _classList[index]['teacherName'],
                        isTeacher: false,
                      ),
                    );
                  }),
        )
      ],
    );
  }
}

class _GradientBackgroundView extends StatelessWidget {
  const _GradientBackgroundView();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            AppTheme.primaryColor,
            Colors.white,
          ],
          stops: [0.064, 0.20],
        ),
      ),
    );
  }
}

class _AddClassDialog extends StatefulWidget {
  const _AddClassDialog();

  @override
  State<_AddClassDialog> createState() => _AddClassDialogState();
}

class _AddClassDialogState extends BaseStateful<_AddClassDialog> {
  String _name = '';
  String _description = '';

  final _db = FirebaseFirestore.instance;

  Future<void> _submit() async {
    final teacher = await _getTeacher();
    String codeClass = _generateClassCode();
    final data = {
      'teacherId': teacher['id'],
      'teacherName': teacher['name'],
      'name': _name,
      'description': _description,
      'code': codeClass
    };

    try {
      await _db.collection('class').doc(codeClass).set(data);
    } catch (e) {
      rethrow;
    }
  }

  Future<Map<String, dynamic>> _getTeacher() async {
    try {
      final uid = FirebaseAuth.instance.currentUser!.uid;
      final snapshot = await _db.collection('users').doc(uid).get();
      return snapshot.data()!;
    } catch (e) {
      rethrow;
    }
  }

  String _generateClassCode() {
    String areaCode = "${Random().nextInt(900) + 100}";
    String subscriberNumber = "${Random().nextInt(900) + 100}";
    return "$areaCode-$subscriberNumber";
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Buat Kelas',
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
              "Buat lingkungan kelas untuk membuat suasana kelas sesuai selera kamu!",
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500)),
          const SizedBox(height: 20),
          CommonInput(
              placeholder: 'Nama Kelas',
              onChanged: (value) {
                _name = value;
              }),
          const SizedBox(height: 10),
          CommonInput(
              placeholder: 'Detail Kelas',
              onChanged: (value) {
                _description = value;
              }),
        ],
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Batal',
              style:
                  TextStyle(color: Colors.grey, fontWeight: FontWeight.w500)),
        ),
        TextButton(
          onPressed: () {
            showLoading();
            _submit();
            hideLoading();
            showSuccessMessage('Anda berhasil membuat kelas');
            Navigator.pop(context);
          },
          child: Text('Tambah',
              style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                  fontWeight: FontWeight.w500)),
        ),
      ],
    );
  }
}
