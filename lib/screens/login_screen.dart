import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:skloai/components/desktop_wrapper.dart';

import '../components/base_stateful.dart';
import '../components/button.dart';
import '../components/input.dart';
import '../utils/routes.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends BaseStateful<LoginScreen> {
  String _email = '';
  String _password = '';
  bool _isObsecurePassword = true;

  Future<void> _login() async {
    try {
      showLoading();
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: _email, password: _password);
      hideLoading();
      Navigator.pushNamedAndRemoveUntil(
          context, Routes.splashScreen, (route) => false);
    } catch (e) {
      showErrorMesssage(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        toolbarHeight: 64,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: const SizedBox(
                width: 32,
                height: 32,
                child: Icon(
                  Icons.arrow_back_ios_new,
                ),
              ),
            ),
            Image.asset('assets/images/logo.png', width: 64),
            const SizedBox(width: 32),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Image.asset('assets/images/undraw_hello.png', width: 296),
                      const SizedBox(height: 24),
                      const Center(
                        child: Text(
                          'Selamat datang di SkolAI',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Poppins',
                          ),
                        ),
                      ),
                      const SizedBox(height: 8),
                      const Text(
                        'Masukkan detail akunmu dibawah ini untuk belajar bersama SkolAI',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Poppins',
                            color: Color(0xFF7B7B7B)),
                      ),
                      const SizedBox(height: 24),
                      CommonInput(
                          placeholder: 'Masukan email',
                          onChanged: (value) => _email = value),
                      const SizedBox(height: 12),
                      CommonInput(
                          placeholder: 'Masukan password',
                          suffixIcon: InkWell(
                            child: _isObsecurePassword
                                ? const Icon(Icons.visibility)
                                : const Icon(Icons.visibility_off),
                            onTap: () => setState(() =>
                                _isObsecurePassword = !_isObsecurePassword),
                          ),
                          obscure: _isObsecurePassword ? true : false,
                          onChanged: (value) => _password = value),
                      const SizedBox(height: 8),
                      GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Text('Lupa kata sandi?',
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Theme.of(context).primaryColor))),
                      const SizedBox(height: 100),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  CommonButton(
                      title: 'Masuk', isPrimary: true, onClick: () => _login()),
                  const SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Belum punya akun?',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFF7B7B7B),
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.pushNamed(context, '/register'),
                        child: Text(' Daftar',
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: 'Poppins',
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w400,
                            )),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
