import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/components/search_field.dart';
import 'package:skloai/utils/routes.dart';

import '../components/desktop_wrapper.dart';
import '../utils/app_theme.dart';

class MindMapListScreen extends StatefulWidget {
  final String lessonId;
  const MindMapListScreen({Key? key, required this.lessonId}) : super(key: key);

  @override
  State<MindMapListScreen> createState() => _MindMapListScreenState();
}

class _MindMapListScreenState extends BaseStateful<MindMapListScreen> {
  final _db = FirebaseFirestore.instance;

  String _lessonTitle = '';
  final List<String> _userListName = [];

  Future<QuerySnapshot<Object?>>? _listOfMindMapFuture;

  Future<QuerySnapshot<Map<String, dynamic>>> _getListMindMap() async {
    try {
      final snapshot = await _db
          .collection('mindmap')
          .where('lesson_id', isEqualTo: widget.lessonId)
          .get();
      for (var doc in snapshot.docs) {
        _userListName.add(await _getUserNameByUserId(doc.data()['user_id']));
      }

      return snapshot;
    } catch (e) {
      rethrow;
    }
  }

  Future<String> _getLessonTitleByLessonId(String lessonId) async {
    try {
      final snapshot = await _db.collection('lessons').doc(lessonId).get();
      if (snapshot.exists && snapshot.data() != null) {
        return snapshot.data()!['title'] ?? 'No Title';
      } else {
        return 'No Title';
      }
    } catch (e) {
      rethrow;
    }
  }

  void _assignTitle() async {
    var title = await _getLessonTitleByLessonId(widget.lessonId);
    setState(() {
      _lessonTitle = title;
    });
  }

  Future<String> _getUserNameByUserId(String uid) async {
    try {
      final snapshot = await _db.collection('users').doc(uid).get();
      if (snapshot.exists && snapshot.data() != null) {
        return snapshot.data()!['name'] ?? 'No Name';
      } else {
        return 'No Name';
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> _addScore(String id) async {
    try {
      showLoading();
      await _db.collection('mindmap').doc(id).update({'score': 100});

      setState(() {
        _listOfMindMapFuture = _getListMindMap();
      });
      hideLoading();
      Navigator.pop(context);
      showSuccessMessage('Berhasil memberi nilai');
    } catch (e) {
      showErrorMesssage('Gagal Memberi Nilai');
      rethrow;
    }
  }

  @override
  void initState() {
    _assignTitle();
    _listOfMindMapFuture = _getListMindMap();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Mind Map Siswa'),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SearchField(),
            const SizedBox(height: 20),
            const Text('Mind Map siswa',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
            const SizedBox(height: 10),
            _buildList(),
          ],
        ),
      ),
    ));
  }

  RefreshIndicator _buildList() {
    return RefreshIndicator(
      onRefresh: () async {
        setState(() {
          _listOfMindMapFuture = _getListMindMap();
        });
      },
      child: FutureBuilder<QuerySnapshot<Object?>>(
          future: _listOfMindMapFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return loadingWidget();
            } else if (snapshot.hasError) {
              return errorWidget(snapshot.error.toString());
            } else {
              List<Map<String, dynamic>> list = [];

              QuerySnapshot<Object?> querySnapshot = snapshot.data!;
              for (QueryDocumentSnapshot document in querySnapshot.docs) {
                Map<String, dynamic> data =
                    document.data()! as Map<String, dynamic>;
                data['id'] = document.id;
                list.add(data);
              }

              if (list.isEmpty) {
                return noDataWidget();
              }

              return SizedBox(
                height: MediaQuery.of(context).size.height - 220,
                child: ListView.separated(
                  padding: const EdgeInsets.only(top: 0),
                  shrinkWrap: true,
                  itemCount: list.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                        onTap: () => Navigator.pushNamed(
                            context, Routes.mindMapScreen,
                            arguments: ScreenArgument<Map<String, dynamic>>(
                                list[index])),
                        child: _buildItem(
                            title: _userListName[index],
                            lessonTitle: _lessonTitle,
                            icons: list[index]['score'] == null
                                ? Icons.info_outline_rounded
                                : Icons.check_circle,
                            colorIcon: list[index]['score'] == null
                                ? Colors.amber
                                : Colors.green,
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return _dialogView(list, index, context);
                                  });
                            }));
                  },
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 10),
                ),
              );
            }
          }),
    );
  }

  AlertDialog _dialogView(
      List<Map<String, dynamic>> list, int index, BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Nilai Mind Map',
        textAlign: TextAlign.center,
      ),
      content: list[index]['score'] != null
          ? Text('Siswa ini sudah mendapatkan nilai ${list[index]['score']}',
              style: Theme.of(context).textTheme.bodyMedium)
          : Text(
              textAlign: TextAlign.justify,
              'Apa anda yakin ingin memberi '
              'nilai kepada ${_userListName[index]}?',
              style: Theme.of(context).textTheme.bodyMedium),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Tutup')),
        ElevatedButton(
            onPressed: () => _addScore(list[index]['id']),
            child: const Text('Beri Nilai'))
      ],
    );
  }

  Widget _buildItem(
          {required String title,
          required String lessonTitle,
          required IconData icons,
          required VoidCallback onPressed,
          required Color colorIcon}) =>
      SizedBox(
        width: double.maxFinite,
        height: 100,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              border: Border.all(color: AppTheme.greyColor)),
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(title,
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w600)),
                      const SizedBox(height: 6),
                      Text(lessonTitle,
                          style: const TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w400)),
                    ]),
                IconButton(
                    onPressed: onPressed,
                    icon: Icon(
                      size: 24,
                      icons,
                      color: colorIcon,
                    ))
              ],
            ),
          ),
        ),
      );
}
