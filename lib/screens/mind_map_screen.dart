import 'dart:convert';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:graphview/GraphView.dart';
import 'package:http/http.dart' as http;
import 'package:skloai/components/base_stateful.dart';

import '../components/desktop_wrapper.dart';

class MindMapScreen extends StatefulWidget {
  final Map<String, dynamic> mindmap;

  const MindMapScreen({Key? key, required this.mindmap}) : super(key: key);

  @override
  State<MindMapScreen> createState() => _MindMapScreenState();
}

class _MindMapScreenState extends BaseStateful<MindMapScreen> {
  final _db = FirebaseFirestore.instance;

  Map<String, dynamic> _json = {};
  bool _isLoading = true;
  String _name = '';
  bool _isStudent = false;

  void _assignMindMap() {
    _json = widget.mindmap['mindmap'];
    _isLoading = false;
    List<Map<String, dynamic>> edges = [];

    for (var element in _json['edges']) {
      edges.add(element as Map<String, dynamic>);
    }

    for (var element in edges) {
      var fromNodeId = element['from'];
      var toNodeId = element['to'];
      graph.addEdge(Node.Id(fromNodeId), Node.Id(toNodeId));
    }

    builder
      ..siblingSeparation = (50)
      ..levelSeparation = (100)
      ..subtreeSeparation = (80)
      ..orientation = (BuchheimWalkerConfiguration.ORIENTATION_LEFT_RIGHT);
  }

  Future<void> _addScore(String id) async {
    try {
      Map data = {
        'teacher_mindmap': '96956307-d958-4b05-bc83-90fb0dd46407',
        'student_mindmap': id
      };
      var body = jsonEncode(data);

      final url = Uri.parse("${dotenv.env['BASE_URL']!}/mindmap/score");
      var response = await http
          .post(url, body: body, headers: {"Content-Type": "application/json"});

      debugPrint(response.statusCode.toString());

      switch (response.statusCode) {
        case 200:
          var decodedResponse =
              jsonDecode(utf8.decode(response.bodyBytes)) as Map;

          await _db
              .collection('mindmap')
              .doc(id)
              .update({'score': decodedResponse['data']['score']});
          final updatedMindmap = await _db.collection('mindmap').doc(id).get();

          setState(() {
            widget.mindmap['score'] = updatedMindmap.data()?['score'] ?? 8;
            _assignMindMap();
          });

          hideLoading();
          Navigator.pop(context);
          showSuccessMessage('Berhasil memberi nilai');
          break;
        default:
          hideLoading();
          Navigator.pop(context);
          throw Exception('Gagal memberi nilai');
      }

      setState(() {});
    } catch (e) {
      hideLoading();
      Navigator.pop(context);
      showErrorMesssage(e.toString());

      setState(() {});
    }
  }

  Future<String> _getUserNameByUserId(String uid) async {
    try {
      final snapshot = await _db.collection('users').doc(uid).get();
      if (snapshot.exists && snapshot.data() != null) {
        return snapshot.data()!['name'] ?? 'No Name';
      } else {
        return 'No Name';
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<bool> _checkStudent() async {
    String uid = FirebaseAuth.instance.currentUser!.uid;

    DocumentSnapshot snapshot =
        await FirebaseFirestore.instance.collection('users').doc(uid).get();
    final data = snapshot.data() as Map<String, dynamic>;

    return data['role'] == 'student';
  }

  @override
  void initState() {
    super.initState();
    _assignMindMap();
    _checkStudent().then((value) {
      setState(() {
        _isStudent = value;
      });
    });
    _getUserNameByUserId(widget.mindmap['user_id'] as String).then((value) {
      setState(() {
        _name = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('MindMap'),
          actions: [
            Builder(builder: (context) {
              if (widget.mindmap['score'] != null) {
                return Center(
                    child: Text('Nilai : ${widget.mindmap['score']}/10',
                        style: Theme.of(context).textTheme.bodyLarge));
              } else {
                debugPrint('${widget.mindmap}');
                if (!_isStudent) {
                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return _showDialogView(
                                    context,
                                    () => _addScore(
                                        widget.mindmap['id'] as String),
                                    _name);
                              });
                        },
                        child: const Text('Beri Nilai')),
                  );
                } else {
                  return const SizedBox();
                }
              }
            }),
            const SizedBox(width: 10),
          ],
        ),
        body: _isLoading
            ? const Center(child: CircularProgressIndicator())
            : InteractiveViewer(
                constrained: false,
                boundaryMargin: const EdgeInsets.all(100),
                minScale: 0.01,
                maxScale: 5.6,
                child: GraphView(
                  graph: graph,
                  algorithm: BuchheimWalkerAlgorithm(
                      builder, TreeEdgeRenderer(builder)),
                  paint: Paint()
                    ..color = Colors.green
                    ..strokeWidth = 1
                    ..style = PaintingStyle.stroke,
                  builder: (Node node) {
                    var a = node.key!.value as int;
                    var nodes = _json['nodes'];
                    var nodeValue =
                        nodes!.firstWhere((element) => element['id'] == a);
                    return rectangleWidget(nodeValue['label'] as String);
                  },
                ),
              ),
      ),
    ));
  }

  Random r = Random();

  Widget rectangleWidget(String a) {
    return InkWell(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          boxShadow: const [
            BoxShadow(spreadRadius: 1, color: Colors.blue),
          ],
        ),
        child: Text(a),
      ),
    );
  }

  final Graph graph = Graph()..isTree = true;
  BuchheimWalkerConfiguration builder = BuchheimWalkerConfiguration();

  AlertDialog _showDialogView(
      BuildContext context, VoidCallback onPressed, String name) {
    return AlertDialog(
      title: const Text(
        'Nilai Mind Map',
        textAlign: TextAlign.center,
      ),
      content: Text(
          textAlign: TextAlign.justify,
          'Apa anda yakin ingin memberi '
          'nilai kepada $name?',
          style: Theme.of(context).textTheme.bodyMedium),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Tutup')),
        ElevatedButton(onPressed: onPressed, child: const Text('Beri Nilai'))
      ],
    );
  }
}
