import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/components/desktop_wrapper.dart';

class NoteScreen extends StatefulWidget {
  final String lessonId;
  const NoteScreen({super.key, required this.lessonId});

  @override
  State<NoteScreen> createState() => _NoteScreenState();
}

class _NoteScreenState extends BaseStateful<NoteScreen> {
  final TextEditingController titleController = TextEditingController();
  final TextEditingController bodyController = TextEditingController();

  String note = '';

  final _uid = FirebaseAuth.instance.currentUser!.uid;

  Future<void> _submit() async {
    showLoading();

    setState(() {
      note = '${titleController.text}\n${bodyController.text}';
    });

    try {
      Map data = {'lesson_id': widget.lessonId, 'user_id': _uid, 'text': note};
      var body = jsonEncode(data);
      final url = Uri.parse("${dotenv.env['BASE_URL']!}/mindmap/create");
      var response = await http
          .post(url, body: body, headers: {"Content-Type": "application/json"});

      debugPrint(response.statusCode.toString());

      switch (response.statusCode) {
        case 200:
          hideLoading();
          Navigator.pop(context);
          Navigator.pop(context);
          showSuccessMessage('Catatan berhasil dikirim');
          break;
        default:
          hideLoading();
          // Navigator.pop(context);
          throw Exception('Gagal menyimpan note');
      }

      setState(() {});
    } catch (e) {
      hideLoading();
      // Navigator.pop(context);
      showErrorMesssage(e.toString());

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return DesktopWrapper(
      wrapper: Scaffold(
        body: SafeArea(
          child: Container(
            height: size.height,
            padding: const EdgeInsets.all(16.0),
            child: Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: const Icon(Icons.arrow_back_ios)),
                  SizedBox(width: MediaQuery.of(context).size.width / 4),
                  const Text("Catatan",
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.w500)),
                ],
              ),
              const SizedBox(height: 20),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      TextFormField(
                          maxLines: null,
                          autofocus: true,
                          controller: titleController,
                          keyboardType: TextInputType.multiline,
                          textCapitalization: TextCapitalization.sentences,
                          decoration: const InputDecoration.collapsed(
                              hintText: "Judul"),
                          style: const TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.w500)),
                      const SizedBox(height: 20),
                      TextFormField(
                        controller: bodyController,
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        textCapitalization: TextCapitalization.sentences,
                        decoration: const InputDecoration.collapsed(
                          hintText: "Ketik di sini...",
                        ),
                        style: const TextStyle(fontSize: 14.0),
                      ),
                    ],
                  ),
                ),
              ),
            ]),
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            if (titleController.text.isEmpty && bodyController.text.isEmpty) {
              _showEmptyTitleDialog(context);
              debugPrint(_uid);
              debugPrint(widget.lessonId);
            } else {
              _buildDialog(context);
            }
          },
          label: const Text("Simpan"),
          icon: const Icon(Icons.save),
        ),
      ),
    );
  }

  Future<dynamic> _buildDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (_) => Dialog(
              backgroundColor: Theme.of(context).colorScheme.surface,
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Container(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text("Yakin ingin menyimpan catatan?",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500)),
                    const SizedBox(height: 20),
                    const Text(
                        "Setelah menyimpan catatan anda tidak bisa mengubahnya kembali",
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w400)),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ElevatedButton(
                          onPressed: () => Navigator.pop(context),
                          style: ElevatedButton.styleFrom(
                              side: const BorderSide(
                                  color: Colors.black, width: 1),
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.black),
                          child: const Text("Batal"),
                        ),
                        const SizedBox(width: 10),
                        ElevatedButton(
                          onPressed: () => _submit(),
                          child: const Text("Simpan"),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ));
  }
}

void _showEmptyTitleDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        backgroundColor: Theme.of(context).colorScheme.surface,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        title: Text("Catatan kosong!",
            style: Theme.of(context).textTheme.titleLarge),
        content: Text('Catatan tidak boleh kosong untuk konversi Mind Map.',
            style: Theme.of(context).textTheme.titleMedium),
        actions: <Widget>[
          TextButton(
            child: Text("Ok", style: Theme.of(context).textTheme.titleMedium),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}
