import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:skloai/components/base_stateful.dart';
import 'package:skloai/components/desktop_wrapper.dart';

import '../components/input.dart';
import '../utils/routes.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends BaseStateful<RegisterScreen> {
  String _email = '';
  String _password = '';
  String _confirmPassword = '';
  bool _isObsecurePassword = true;
  bool _isObsecureConfirmPassword = true;

  Future<void> _nextStep(int value) async {
    showLoading();

    if (_email.isEmpty || _password.isEmpty || _confirmPassword.isEmpty) {
      hideLoading();
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Semua form wajib diisi')));
      return;
    }

    hideLoading();

    if (_password != _confirmPassword) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Password tidak sama')));
      return;
    }

    Navigator.pushNamed(
        context,
        value == 0
            ? Routes.registerStudentScreen
            : Routes.registerTeacherScreen,
        arguments: {'email': _email, 'password': _password});
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        toolbarHeight: 64,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: const SizedBox(
                width: 32,
                height: 32,
                child: Icon(
                  Icons.arrow_back_ios_new,
                ),
              ),
            ),
            const Text(
              'Buat Akun',
              style: TextStyle(
                fontSize: 16,
                fontFamily: 'Poppins',
              ),
            ),
            const SizedBox(width: 32),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Bergabung agar belajar kamu menjadi lebih mudah dan menyenangkan',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(height: 8),
              const Text(
                'Untuk mendaftar di SkolAI, mari isi menggunakan email aktif dan kata sandi yang unik yaaa!!',
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF7B7B7B),
                ),
              ),
              const SizedBox(height: 40),
              CommonInput(
                  placeholder: 'Masukan email',
                  onChanged: (value) => _email = value),
              const SizedBox(height: 24),
              CommonInput(
                  placeholder: 'Masukan password',
                  suffixIcon: InkWell(
                    child: _isObsecurePassword
                        ? const Icon(Icons.visibility)
                        : const Icon(Icons.visibility_off),
                    onTap: () => setState(
                        () => _isObsecurePassword = !_isObsecurePassword),
                  ),
                  obscure: _isObsecurePassword ? true : false,
                  onChanged: (value) => _password = value),
              const SizedBox(height: 24),
              CommonInput(
                  placeholder: 'Konfirmasi password',
                  suffixIcon: InkWell(
                    child: _isObsecureConfirmPassword
                        ? const Icon(Icons.visibility)
                        : const Icon(Icons.visibility_off),
                    onTap: () => setState(() => _isObsecureConfirmPassword =
                        !_isObsecureConfirmPassword),
                  ),
                  obscure: _isObsecureConfirmPassword ? true : false,
                  onChanged: (value) => _confirmPassword = value),
              const SizedBox(height: 24),
              const Text(
                'Daftar Sebagai Apa?',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 8),
              const Text(
                'Pilih peran kamu untuk mendaftar sebagai apa!Pilih peran kamu untuk mendaftar sebagai apa!',
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF7B7B7B),
                ),
              ),
              const SizedBox(height: 16),
              SizedBox(
                height: 48,
                width: double.infinity,
                child: Padding(
                  padding: EdgeInsets.zero,
                  child: CupertinoSegmentedControl(children: const {
                    0: Padding(
                      padding: EdgeInsets.all(12),
                      child: Text('Siswa'),
                    ),
                    1: Padding(
                      padding: EdgeInsets.all(12),
                      child: Text('Guru'),
                    ),
                  }, onValueChanged: (int value) => _nextStep(value)),
                ),
              ),
              const SizedBox(height: 40),
              // CommonButton(
              //     title: 'Berikutnya',
              //     isPrimary: true,
              //     onClick: () {
              //       if (_password != _confirmPassword) {
              //         ScaffoldMessenger.of(context).showSnackBar(
              //             const SnackBar(content: Text('Password tidak sama')));
              //         return;
              //       }
              //       FirebaseAuth.instance
              //           .createUserWithEmailAndPassword(
              //               email: _email, password: _password)
              //           .then((value) {
              //         FirebaseFirestore.instance
              //             .collection('users')
              //             .doc(value.user!.uid)
              //             .set({
              //           'email': _email,
              //           'role': 'student',
              //         });
              //         Navigator.pushNamedAndRemoveUntil(
              //             context, Routes.loginScreen, (route) => false);
              //       }).catchError((error) {
              //         ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              //           content: Text(error.toString()),
              //         ));
              //       });
              //     }),
            ],
          ),
        ),
      ),
    ));
  }
}
