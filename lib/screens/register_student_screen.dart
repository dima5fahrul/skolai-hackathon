import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:skloai/components/desktop_wrapper.dart';

import '../components/base_stateful.dart';
import '../components/button.dart';
import '../components/input.dart';
import '../utils/routes.dart';

class RegisterStudentScreen extends StatefulWidget {
  final String email;
  final String password;

  const RegisterStudentScreen(
      {Key? key, required this.email, required this.password})
      : super(key: key);

  @override
  State<RegisterStudentScreen> createState() => _RegisterStudentScreenState();
}

class _RegisterStudentScreenState extends BaseStateful<RegisterStudentScreen> {
  String _email = '';
  String _password = '';
  String _name = '';
  String _nisn = '';
  String _whatsapp = '';
  String _educationLevel = '';

  Future<void> _register() async {
    try {
      showLoading();
      _email = widget.email;
      _password = widget.password;
      final user = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: _email, password: _password);
      await FirebaseFirestore.instance
          .collection('users')
          .doc(user.user!.uid)
          .set({
        'id': user.user!.uid,
        'name': _name,
        'whatsapp': _whatsapp,
        'nisn': _nisn,
        'education_level': _educationLevel,
        'role': 'student',
        'class': FieldValue.arrayUnion([])
      });
      hideLoading();
      showSuccessMessage('Anda berhasil membuat akun');
      Navigator.pushNamedAndRemoveUntil(
          context, Routes.loginScreen, (route) => false);
    } catch (e) {
      debugPrint(e.toString());
      showErrorMesssage(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        toolbarHeight: 64,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: const SizedBox(
                width: 32,
                height: 32,
                child: Icon(
                  Icons.arrow_back_ios_new,
                ),
              ),
            ),
            const Text(
              'Buat Akun',
              style: TextStyle(
                fontSize: 16,
                fontFamily: 'Poppins',
              ),
            ),
            const SizedBox(width: 32),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(32.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Kenalan dulu yuk :D',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Poppins',
                ),
              ),
              const SizedBox(height: 8),
              const Text(
                  'Sebelum belajar bersama, mari isi informasi diri kamu yuk agar SkolAI dapat mengenalimu dengan lebih baik!',
                  style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF7B7B7B))),
              const SizedBox(height: 40),
              CommonInput(
                  placeholder: 'Nama lengkap',
                  onChanged: (value) => _name = value),
              const SizedBox(height: 24),
              CommonInput(
                  placeholder: 'NISN', onChanged: (value) => _nisn = value),
              const SizedBox(height: 24),
              CommonInput(
                  placeholder: 'No whatsapp',
                  onChanged: (value) => _whatsapp = value),
              const SizedBox(height: 24),
              CommonInput(
                  placeholder: 'Jenjang pendidikan',
                  onChanged: (value) => _educationLevel = value),
              const SizedBox(height: 120),
              CommonButton(title: 'Daftar', onClick: () => _register()),
            ],
          ),
        ),
      ),
    ));
  }
}
