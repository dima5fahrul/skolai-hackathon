import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../components/desktop_wrapper.dart';
import '../utils/routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 2), initial);
  }

  Future<void> initial() async {
    User? user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      bool isStudent = await _isStudent(user.uid);
      if (isStudent) {
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.studentNavbarView, (route) => false);
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.teacherNavbarView, (route) => false);
      }
    } else {
      Navigator.pushNamedAndRemoveUntil(
          context, Routes.authScreen, (route) => false);
    }
  }

  Future<bool> _isStudent(String uid) async {
    DocumentSnapshot snapshot =
        await FirebaseFirestore.instance.collection('users').doc(uid).get();
    final data = snapshot.data() as Map<String, dynamic>;

    return data['role'] == 'student';
  }

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      body: Center(
        child: Image.asset('assets/images/logo.png'),
      ),
    ));
  }
}
