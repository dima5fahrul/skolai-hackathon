import 'package:flutter/material.dart';
import 'package:skloai/screens/class_screen.dart';
import 'package:skloai/screens/home_student_screen.dart';
import 'package:skloai/utils/app_theme.dart';

import '../components/desktop_wrapper.dart';
import '../components/fab_bottom_bar.dart';

class StudentNavbarView extends StatefulWidget {
  const StudentNavbarView({super.key});

  @override
  State<StudentNavbarView> createState() => _NavbarViewState();
}

class _NavbarViewState extends State<StudentNavbarView> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: FABBottomBar(
        iconSize: 32,
        color: Colors.grey,
        selectedColor: AppTheme.primaryColor,
        backgroundColor: Colors.white,
        onTabSelected: (index) {
          setState(() => _currentIndex = index);
        },
        items: [
          FABBottomBarItem(icon: Icons.home_outlined, title: 'Beranda'),
          FABBottomBarItem(icon: Icons.school_outlined, title: 'Kelas'),
          FABBottomBarItem(icon: Icons.account_tree_outlined, title: 'MindMap'),
          FABBottomBarItem(icon: Icons.person_outline, title: 'Profil'),
        ],
      ),
      body: IndexedStack(
        index: _currentIndex,
        children: const [
          HomeStudentScreen(),
          ClassScreen(),
          HomeStudentScreen(),
          HomeStudentScreen(),
        ],
      ),
    ));
  }
}
