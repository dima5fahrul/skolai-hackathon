import 'package:flutter/material.dart';
import 'package:skloai/components/desktop_wrapper.dart';
import 'package:skloai/screens/home_student_screen.dart';
import 'package:skloai/screens/home_teacher_screen.dart';
import 'package:skloai/utils/app_theme.dart';

import '../components/fab_bottom_bar.dart';

class TeacherNavbarView extends StatefulWidget {
  const TeacherNavbarView({super.key});

  @override
  State<TeacherNavbarView> createState() => _NavbarViewState();
}

class _NavbarViewState extends State<TeacherNavbarView> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return DesktopWrapper(
        wrapper: Scaffold(
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: FABBottomBar(
        iconSize: 32,
        color: Colors.grey,
        selectedColor: AppTheme.primaryColor,
        backgroundColor: Colors.white,
        onTabSelected: (index) => setState(() => _currentIndex = index),
        items: [
          FABBottomBarItem(icon: Icons.home_outlined, title: 'Beranda'),
          FABBottomBarItem(icon: Icons.school_outlined, title: 'Kelas'),
          FABBottomBarItem(icon: Icons.account_tree_outlined, title: 'MindMap'),
          FABBottomBarItem(icon: Icons.person_outline, title: 'Profil'),
        ],
      ),
      body: IndexedStack(
        index: _currentIndex,
        children: const [
          HomeTeacherScreen(),
          HomeTeacherScreen(),
          HomeStudentScreen(),
          HomeStudentScreen(),
        ],
      ),
    ));
  }
}
