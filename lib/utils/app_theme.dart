import 'package:flutter/material.dart';

class AppTheme {
  static const Color primaryColor = Color(0xFF4361EE);
  static const Color secondaryColor = Color(0xFFF77F00);
  static const Color greyColor = Color(0xFFCAC4D0);

  static ThemeData theme() {
    return ThemeData(
      fontFamily: 'Poppins',
      scaffoldBackgroundColor: const Color(0xFFEDF2FB),
      primaryColor: primaryColor,
      appBarTheme: const AppBarTheme(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          titleTextStyle: TextStyle(
              fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black),
          centerTitle: false,
          elevation: 1),
      colorScheme: ColorScheme.fromSeed(
          secondary: secondaryColor, seedColor: primaryColor),
      useMaterial3: false,
    );
  }
}
