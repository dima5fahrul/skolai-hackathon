import 'package:flutter/material.dart';
import 'package:skloai/screens/mind_map_screen.dart';
import 'package:skloai/screens/note_screen.dart';

import '../screens/add_lesson_screen.dart';
import '../screens/auth_screen.dart';
import '../screens/chat_screen.dart';
import '../screens/detail_class_screen.dart';
import '../screens/detail_lesson_screen.dart';
import '../screens/home_student_screen.dart';
import '../screens/home_teacher_screen.dart';
import '../screens/login_screen.dart';
import '../screens/mind_map_list_screen.dart';
import '../screens/register_screen.dart';
import '../screens/register_student_screen.dart';
import '../screens/register_teacher_screen.dart';
import '../screens/splash_screen.dart';
import '../screens/student_navbar_view.dart';
import '../screens/teacher_navbar_view.dart';

class Routes {
  static const String splashScreen = '/';
  static const String authScreen = '/auth';
  static const String loginScreen = '/login';
  static const String registerScreen = '/register';
  static const String registerStudentScreen = '/register/student/';
  static const String registerTeacherScreen = '/register/teacher/';
  static const String homeStudentScreen = '/student/';
  static const String homeTeacherScreen = '/teacher/';
  static const String detailClassScreen = '/class/detail';
  static const String detailLessonScreen = '/lesson/detail';
  static const String addLessonScreen = '/lesson/add';
  static const String chatScreen = '/lesson/chat';
  static const String mindMapScreen = '/mindmap';
  static const String mindMapListScreen = '/mindmap/list';
  static const String noteScreen = '/note';
  static const String profileScreen = '/profile';
  static const String studentNavbarView = '/student/navbar';
  static const String teacherNavbarView = '/teacher/navbar';

  static Route<dynamic> generate(RouteSettings settings) {
    switch (settings.name) {
      case splashScreen:
        return MaterialPageRoute(builder: (_) => const SplashScreen());
      case authScreen:
        return MaterialPageRoute(builder: (_) => const AuthScreen());
      case loginScreen:
        return MaterialPageRoute(builder: (_) => const LoginScreen());
      case registerScreen:
        return MaterialPageRoute(builder: (_) => const RegisterScreen());
      case studentNavbarView:
        return MaterialPageRoute(builder: (_) => const StudentNavbarView());
      case teacherNavbarView:
        return MaterialPageRoute(builder: (_) => const TeacherNavbarView());
      case registerStudentScreen:
        return MaterialPageRoute(
          builder: (context) {
            final args = settings.arguments as Map<String, String>;
            return RegisterStudentScreen(
              email: args['email']!,
              password: args['password']!,
            );
          },
        );
      case registerTeacherScreen:
        return MaterialPageRoute(
          builder: (context) {
            final args = settings.arguments as Map<String, String>;
            return RegisterTeacherScreen(
              email: args['email']!,
              password: args['password']!,
            );
          },
        );
      case homeStudentScreen:
        return MaterialPageRoute(builder: (_) => const HomeStudentScreen());
      case homeTeacherScreen:
        return MaterialPageRoute(builder: (_) => const HomeTeacherScreen());
      case detailClassScreen:
        final args = settings.arguments as ScreenArgument<Map<String, dynamic>>;
        return MaterialPageRoute(
            builder: (_) => DetailClassScreen(
                  data: args.data,
                ));
      case addLessonScreen:
        final args = settings.arguments as ScreenArgument<String>;
        return MaterialPageRoute(
            builder: (_) => AddLessonScreen(
                  classId: args.data,
                ));
      case detailLessonScreen:
        final args = settings.arguments as ScreenArgument<Map<String, dynamic>>;
        return MaterialPageRoute(
            builder: (_) => DetailLessonScreen(
                  lesson: args.data,
                ));
      case chatScreen:
        final args = settings.arguments as ScreenArgument<String>;
        return MaterialPageRoute(
            builder: (_) => ChatScreen(lessonId: args.data));
      case noteScreen:
        final args = settings.arguments as ScreenArgument<String>;
        return MaterialPageRoute(
            builder: (_) => NoteScreen(lessonId: args.data));
      case mindMapScreen:
        final args = settings.arguments as ScreenArgument<Map<String, dynamic>>;
        return MaterialPageRoute(
            builder: (_) => MindMapScreen(
                  mindmap: args.data,
                ));
      case mindMapListScreen:
        final args = settings.arguments as ScreenArgument<String>;
        return MaterialPageRoute(
            builder: (_) => MindMapListScreen(
                  lessonId: args.data,
                ));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}

class ScreenArgument<T> {
  final T data;
  ScreenArgument(this.data);
}
